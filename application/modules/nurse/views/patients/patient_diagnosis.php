<div class="row">
  <div class="col-md-6">
    <section class="panel panel-featured panel-featured-info">
      <header class="panel-heading">
        <h2 class="panel-title">Diagnosis</h2>
      </header>

      <div class="panel-body">
                <div class="col-lg-8 col-md-8 col-sm-8">
                  <div class="form-group">
                    <select id='diseases_id' name='diseases_id' class='form-control custom-select ' >
                      <option value=''>None - Please Select a diagnosis</option>
                      <?php echo $diseases;?>
                    </select>
                  </div>
                
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4">
                  <div class="form-group">
                      <button type='submit' class="btn btn-sm btn-success"  onclick="pass_diagnosis(<?php echo $visit_id;?>);"> Identify the disease</button>
                  </div>
                </div>
      </div>
      <!-- visit Procedures from java script -->
     
      <!-- end of visit procedures -->
      <div id="patient_diagnosis"></div>

            
    </section>
    </div>
    <div class="col-md-6">
      <div class="row">
   <div class="col-md-12">
    <!-- Widget -->
    <section class="panel panel-featured panel-featured-info">
          <header class="panel-heading">
              <h2 class="panel-title">Diagnosis</h2>
          </header>
          <div class="panel-body">
            <div class="padd">
                <?php $data['visit_id'] = $visit_id;?>
               <!-- vitals from java script -->
               <?php echo $this->load->view("nurse/soap/view_diagnosis", $data, TRUE); ?>
               
               <!-- end of vitals data -->
            </div>
          </div>
      </section>
    </div>
 </div>
    </div>
</div>